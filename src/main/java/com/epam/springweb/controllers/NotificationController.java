package com.epam.springweb.controllers;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.springweb.hibernate.entities.Notification;
import com.epam.springweb.hibernate.entities.Topic;
import com.epam.springweb.hibernate.entities.UserProfile;
import com.epam.springweb.service.ifaces.NotificationService;
import com.epam.springweb.service.ifaces.TopicService;
import com.epam.springweb.utils.ApplicationContextUtil;

@Controller
@RequestMapping(value = "/forum")
public class NotificationController {

	@RequestMapping(value = "/send-invitation/{id}", method = RequestMethod.POST)
	public String sendInvitation(@ModelAttribute("receiver") UserProfile receiver, @PathVariable int id) {
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		SecurityContext secContext = SecurityContextHolder.getContext();

		Notification notification = new Notification();
		notification.setTopic(new Topic(id));
		notification.setSender(new UserProfile(secContext.getAuthentication().getName()));
		notification.setReceiver(receiver);

		NotificationService notificationService = (NotificationService) appContext.getBean("notificationService");
		notificationService.addNotification(notification);

		return "redirect:/forum/topic/" + id;
	}

	@RequestMapping(value = "/notifications", method = RequestMethod.GET)
	public String notificationsPage(Model model) {
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		SecurityContext secContext = SecurityContextHolder.getContext();

		String username = secContext.getAuthentication().getName();

		NotificationService notificationService = (NotificationService) appContext.getBean("notificationService");
		List<Notification> notifications = notificationService.getNotifications(username);

		model.addAttribute("notifications", notifications);

		return "content/notificationsPage";
	}

	@RequestMapping(value = "/accept-invite/{id}/{topicId}", method = RequestMethod.POST)
	public String acceptInvite(@PathVariable("id") int id, @PathVariable("topicId") int topicId) {
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		SecurityContext secContext = SecurityContextHolder.getContext();

		String username = secContext.getAuthentication().getName();

		TopicService topicService = (TopicService) appContext.getBean("topicService");
		NotificationService notificationService = (NotificationService) appContext.getBean("notificationService");

		topicService.addTopicMember(new UserProfile(username), topicId);
		notificationService.removeNotification(new Notification(id));

		return "redirect:/forum/notifications";
	}

	@RequestMapping(value = "/decline-invite/{id}", method = RequestMethod.POST)
	public String declineInvite(@PathVariable int id) {
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();

		NotificationService notificationService = (NotificationService) appContext.getBean("notificationService");
		notificationService.removeNotification(new Notification(id));

		return "redirect:/forum/notifications";
	}
}
