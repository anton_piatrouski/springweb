package com.epam.springweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

	@RequestMapping(value = "/form-login", method = RequestMethod.GET)
	public ModelAndView loginForm(@RequestParam(value = "error", required = false) String error) {

		ModelAndView modelAndView = new ModelAndView();

		if ("true".equals(error)) {
			modelAndView.addObject("error", "Incorrect username or password.");
		}
		modelAndView.setViewName("loginForm");

		return modelAndView;
	}

}
