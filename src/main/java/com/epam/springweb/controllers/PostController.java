package com.epam.springweb.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.springweb.hibernate.entities.Comment;
import com.epam.springweb.hibernate.entities.Post;
import com.epam.springweb.hibernate.entities.Topic;
import com.epam.springweb.hibernate.entities.UserProfile;
import com.epam.springweb.service.ifaces.CommentService;
import com.epam.springweb.service.ifaces.PostService;
import com.epam.springweb.service.ifaces.TopicService;
import com.epam.springweb.utils.ApplicationContextUtil;
import com.epam.springweb.utils.PaginationUtil;

@Controller
@RequestMapping(value = "/forum")
public class PostController {

	@RequestMapping(value = "/topic/{id}", method = RequestMethod.GET)
	public ModelAndView topicPage(@PathVariable int id,
			@RequestParam(value = "page", defaultValue = "1") Integer page) {
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		PostService postService = (PostService) appContext.getBean("postService");
		TopicService topicService = (TopicService) appContext.getBean("topicService");

		List<Post> posts = postService.getPosts(id, page);
		Topic topic = topicService.getTopicById(id);
		Set<UserProfile> members = topic.getMembers();

		SecurityContext secContext = SecurityContextHolder.getContext();
		UserProfile user = new UserProfile(secContext.getAuthentication().getName());

		ModelAndView mav = new ModelAndView();
		mav.addObject("posts", posts);
		mav.addObject("topic", topic);
		mav.addObject("post", new Post());
		mav.addObject("receiver", new UserProfile());

		if (members.contains(user)) {
			mav.addObject("isMember", true);
		}
		if (user.equals(topic.getCreator())) {
			mav.addObject("isCreator", true);
		}

		mav.setViewName("content/topicPage");

		return mav;
	}

	@RequestMapping(value = "/post/{postId}", method = RequestMethod.GET)
	public ModelAndView postPage(@PathVariable int postId, Model model) {
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		SecurityContext secContext = SecurityContextHolder.getContext();

		PostService postService = (PostService) appContext.getBean("postService");
		CommentService commentService = (CommentService) appContext.getBean("commentService");

		Post post = postService.getPost(postId);
		Set<UserProfile> members = post.getTopic().getMembers();
		UserProfile user = new UserProfile(secContext.getAuthentication().getName());

		ModelAndView mav = new ModelAndView();
		mav.addObject("post", post);
		mav.addObject("comment", new Comment());
		mav.addObject("comments", commentService.getComments(postId));

		if (members.contains(user)) {
			mav.addObject("canAddComment", true);
		}
		if (user.equals(post.getCreator())) {
			mav.addObject("canDeleteComment", true);
		}

		mav.setViewName("content/postPage");

		return mav;
	}

	@RequestMapping(value = "/feed", method = RequestMethod.GET)
	public ModelAndView feedPage(@RequestParam(value = "page", defaultValue = "1") Integer page) {
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		SecurityContext secContext = SecurityContextHolder.getContext();

		PostService postService = (PostService) appContext.getBean("postService");

		String username = secContext.getAuthentication().getName();
		List<Post> posts = postService.getPostsByMember(username, page);

		int countPosts = postService.getCountPostsByMember(username);
		int pages = PaginationUtil.calcNumberOfPages(countPosts);

		ModelAndView mav = new ModelAndView("content/feedPage");
		mav.addObject("posts", posts);
		mav.addObject("pages", pages);
		mav.addObject("currentPage", page);

		return mav;
	}

}
