package com.epam.springweb.controllers;

import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.epam.springweb.hibernate.entities.Topic;
import com.epam.springweb.hibernate.entities.UserProfile;
import com.epam.springweb.service.ifaces.TopicService;
import com.epam.springweb.utils.ApplicationContextUtil;

@Controller
@RequestMapping(value = "/forum")
public class TopicController {

	@RequestMapping(value = "/form-create-topic", method = RequestMethod.GET)
	public String topicCreationForm(Model model) {
		model.addAttribute("topic", new Topic());

		return "content/topicCreationForm";
	}

	@RequestMapping(value = "/create-topic", method = RequestMethod.POST)
	public void createTopic(@ModelAttribute("topic") Topic topic) {
		SecurityContext secContext = SecurityContextHolder.getContext();
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();

		String username = secContext.getAuthentication().getName();
		UserProfile user = new UserProfile(username);
		topic.setCreator(user);
		topic.addMember(user);

		TopicService topicService = (TopicService) appContext.getBean("topicService");
		topicService.addTopic(topic);
	}

	@RequestMapping(value = "/leave-topic/{id}", method = RequestMethod.GET)
	public String leaveTopic(@PathVariable int id) {
		SecurityContext secContext = SecurityContextHolder.getContext();
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();

		String username = secContext.getAuthentication().getName();
		UserProfile user = new UserProfile(username);

		TopicService topicService = (TopicService) appContext.getBean("topicService");
		topicService.removeTopicMember(user, id);

		return "redirect:/forum";
	}

	@RequestMapping(value = "/join-topic/{id}", method = RequestMethod.GET)
	public String joinTopic(@PathVariable int id) {
		SecurityContext secContext = SecurityContextHolder.getContext();
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();

		String username = secContext.getAuthentication().getName();
		UserProfile user = new UserProfile(username);

		TopicService topicService = (TopicService) appContext.getBean("topicService");
		topicService.addTopicMember(user, id);

		return "redirect:/forum";
	}

	// Modify topic
	@RequestMapping(value = "/form-modify-topic/{id}", method = RequestMethod.GET)
	public ModelAndView topicModifyForm(@PathVariable int id) {
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		TopicService topicService = (TopicService) appContext.getBean("topicService");

		Topic topic = topicService.getTopicById(id);

		ModelAndView mav = new ModelAndView();
		mav.addObject("topic", topic);
		mav.addObject("modifiedTopic", new Topic(id));
		mav.setViewName("content/topicModifyForm");

		return mav;
	}

	@RequestMapping(value = "/modify-topic", method = RequestMethod.POST)
	public String modifyTopic(@ModelAttribute("modifiedTopic") Topic topic) {
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		TopicService topicService = (TopicService) appContext.getBean("topicService");

		SecurityContext secContext = SecurityContextHolder.getContext();
		UserProfile user = new UserProfile(secContext.getAuthentication().getName());

		Set<UserProfile> members = topicService.getTopicMembers(topic.getId());

		topic.setCreator(user);
		topic.setMembers(members);

		topicService.modifyTopic(topic);

		return "redirect:/forum/topic/" + topic.getId();
	}
}
