package com.epam.springweb.controllers;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.springweb.hibernate.entities.Topic;
import com.epam.springweb.hibernate.entities.UserProfile;
import com.epam.springweb.service.ifaces.TopicService;
import com.epam.springweb.utils.ApplicationContextUtil;
import com.epam.springweb.utils.PaginationUtil;

@Controller
@RequestMapping(value = "/forum")
public class ForumController {

	@RequestMapping(method = RequestMethod.GET)
	public String topicsPage(@RequestParam(value = "page", required = false) Integer page, Model model) {
		if (page == null) {
			page = 1;
		}
		SecurityContext secContext = SecurityContextHolder.getContext();
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		TopicService topicService = (TopicService) appContext.getBean("topicService");

		String username = secContext.getAuthentication().getName();
		UserProfile user = new UserProfile(username);

		List<Topic> topics = topicService.getTopics(user, page);
		int numberOfTopics = topicService.getNumberOfTopics(user);
		int numberOfPages = PaginationUtil.calcNumberOfPages(numberOfTopics);

		model.addAttribute("topics", topics);
		model.addAttribute("pages", numberOfPages);
		model.addAttribute("currentPage", page);

		return "content/topics";
	}

	@RequestMapping(value = "/search-topics", method = RequestMethod.GET)
	public String searchTopicsPage() {
		return "content/searchTopics";
	}

	@RequestMapping(value = "/search-topics/results", method = RequestMethod.GET)
	public String searchTopicsResults(@RequestParam(value = "topicName") String topicName,
			@RequestParam(value = "page", required = false) Integer page, Model model) {
		if (page == null) {
			page = 1;
		}
		SecurityContext secContext = SecurityContextHolder.getContext();
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		TopicService topicService = (TopicService) appContext.getBean("topicService");

		String username = secContext.getAuthentication().getName();
		// Topics where user is member in.
		List<Topic> userTopics = topicService.getUserTopics(username);

		List<Topic> topics = topicService.searchTopics(topicName, page);
		int numberOfTopics = topicService.getNumberOfTopicsByName(topicName);
		int numberOfPages = PaginationUtil.calcNumberOfPages(numberOfTopics);

		model.addAttribute("topics", topics);
		model.addAttribute("pages", numberOfPages);
		model.addAttribute("currentPage", page);
		model.addAttribute("topicName", topicName);
		model.addAttribute("userTopics", userTopics);

		if (topics.isEmpty()) {
			model.addAttribute("message", "There are no such topics.");
		}

		return "content/searchTopics";
	}

}
