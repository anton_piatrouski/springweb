package com.epam.springweb.controllers;

import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.springweb.hibernate.entities.Comment;
import com.epam.springweb.hibernate.entities.Post;
import com.epam.springweb.hibernate.entities.UserProfile;
import com.epam.springweb.service.ifaces.CommentService;
import com.epam.springweb.utils.ApplicationContextUtil;

@Controller
@RequestMapping(value = "/forum")
public class CommentController {

	@RequestMapping(value = "/post/{postId}/add-comment", method = RequestMethod.POST)
	public String addComment(@PathVariable("postId") int postId, @ModelAttribute("comment") Comment comment) {
		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		SecurityContext secContext = SecurityContextHolder.getContext();

		String creator = secContext.getAuthentication().getName();

		comment.setCreator(new UserProfile(creator));
		comment.setPost(new Post(postId));

		CommentService commentService = (CommentService) appContext.getBean("commentService");
		commentService.addComment(comment);

		return "redirect:/forum/post/" + postId;
	}

	@RequestMapping(value = "/post/{postId}/add-comment/{parentCommentId}", method = RequestMethod.POST)
	public String addChildComment(@PathVariable("postId") int postId,
			@PathVariable("parentCommentId") int parentCommentId, @ModelAttribute("comment") Comment comment) {

		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		SecurityContext secContext = SecurityContextHolder.getContext();

		String creator = secContext.getAuthentication().getName();

		comment.setCreator(new UserProfile(creator));
		comment.setPost(new Post(postId));
		comment.setParentComment(new Comment(parentCommentId));

		CommentService commentService = (CommentService) appContext.getBean("commentService");
		commentService.addComment(comment);

		return "redirect:/forum/post/" + postId;
	}

	@RequestMapping(value = "/post/{postId}/delete-comment/{commentId}", method = RequestMethod.POST)
	public String deleteComment(@PathVariable("postId") int postId, @PathVariable("commentId") int commentId) {

		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();

		CommentService commentService = (CommentService) appContext.getBean("commentService");
		commentService.removeComment(commentId);

		return "redirect:/forum/post/" + postId;
	}
}
