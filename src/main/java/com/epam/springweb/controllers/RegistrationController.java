package com.epam.springweb.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.springweb.exceptions.UserDaoException;
import com.epam.springweb.objects.UserDto;
import com.epam.springweb.service.ifaces.UserService;
import com.epam.springweb.utils.ApplicationContextUtil;

@Controller
public class RegistrationController {

	@RequestMapping(value = "/form-registration", method = RequestMethod.GET)
	public String registrationForm(@RequestParam(value = "message", required = false) String message, Model model) {
		if (message != null) {
			model.addAttribute("message", message);
		}
		return "registrationForm";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(HttpServletRequest request, Model model) {
		String name = request.getParameter("username");
		String password = request.getParameter("password");

		UserDto userDto = new UserDto(name, password);

		ApplicationContext appContext = ApplicationContextUtil.getApplicationContext();
		UserService userService = (UserService) appContext.getBean("userService");

		try {
			userService.addUser(userDto);

		} catch (UserDaoException e) {
			model.addAttribute("message", e.getMessage());
			return "redirect:/form-registration";
		}

		return "redirect:/form-login";
	}

}
