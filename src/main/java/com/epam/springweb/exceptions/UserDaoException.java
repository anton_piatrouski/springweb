package com.epam.springweb.exceptions;

public class UserDaoException extends Exception {
	private static final long serialVersionUID = 6156605047502879745L;

	private Exception detail;

	public UserDaoException(String message) {
		super(message);
		initCause(null); // Disallow subsequent initCause
	}

	public UserDaoException(String message, Exception cause) {
		super(message);
		initCause(null); // Disallow subsequent initCause
		detail = cause;
	}

	@Override
	public String getMessage() {
		if (detail == null) {
			return super.getMessage();
		} else {
			return super.getMessage() + "; nested exception is: \n\t" + detail.toString();
		}
	}
}
