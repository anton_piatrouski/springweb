package com.epam.springweb.utils;

public class PaginationUtil {

	public static final int SIZE = 10;

	public static int calcFirstResult(int page) {
		return (page - 1) * SIZE;
	}

	public static int calcNumberOfPages(int numberOfRecords) {
		double number = numberOfRecords;
		return (int) Math.ceil(number / SIZE);
	}

}
