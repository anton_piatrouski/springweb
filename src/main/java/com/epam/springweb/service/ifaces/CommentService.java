package com.epam.springweb.service.ifaces;

import java.util.List;

import com.epam.springweb.hibernate.entities.Comment;

public interface CommentService {
	void addComment(Comment comment);

	List<Comment> getComments(int postId);

	void removeComment(int id);
}
