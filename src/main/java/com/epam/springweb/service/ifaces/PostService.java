package com.epam.springweb.service.ifaces;

import java.util.List;

import com.epam.springweb.hibernate.entities.Post;

public interface PostService {
	List<Post> getPosts(int topicId, int page);

	Post getPost(int id);

	List<Post> getPostsByMember(String username, int page);

	void addPost(Post post);

	int getCountPostsByMember(String username);
}
