package com.epam.springweb.service.ifaces;

import java.util.List;
import java.util.Set;

import com.epam.springweb.hibernate.entities.Topic;
import com.epam.springweb.hibernate.entities.UserProfile;

public interface TopicService {

	List<Topic> getTopics(UserProfile user, int page);

	List<Topic> searchTopics(String topicName, int page);

	List<Topic> getUserTopics(String username);

	Set<UserProfile> getTopicMembers(int id);

	Topic getTopicById(int id);

	void addTopic(Topic topic);

	int getNumberOfTopics(UserProfile user);

	int getNumberOfTopicsByName(String topicName);

	void removeTopicMember(UserProfile user, int topicId);

	void addTopicMember(UserProfile user, int topicId);

	void modifyTopic(Topic topic);

}
