package com.epam.springweb.service.ifaces;

import com.epam.springweb.exceptions.UserDaoException;
import com.epam.springweb.objects.UserDto;

public interface UserService {
	void addUser(UserDto userDto) throws UserDaoException;
}
