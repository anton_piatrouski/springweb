package com.epam.springweb.service.ifaces;

import java.util.List;

import com.epam.springweb.hibernate.entities.Notification;

public interface NotificationService {
	void addNotification(Notification notification);

	void removeNotification(Notification notification);

	List<Notification> getNotifications(String username);
}
