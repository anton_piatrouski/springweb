package com.epam.springweb.service.impls;

import java.util.List;

import com.epam.springweb.dao.ifaces.CommentDao;
import com.epam.springweb.hibernate.entities.Comment;
import com.epam.springweb.service.ifaces.CommentService;

public class CommentServiceImpl implements CommentService {

	private CommentDao commentDao;

	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	@Override
	public void addComment(Comment comment) {
		commentDao.insertComment(comment);
	}

	@Override
	public List<Comment> getComments(int postId) {
		return commentDao.selectComments(postId);
	}

	@Override
	public void removeComment(int id) {
		commentDao.deleteComment(id);
	}

}
