package com.epam.springweb.service.impls;

import com.epam.springweb.dao.ifaces.UserDao;
import com.epam.springweb.exceptions.UserDaoException;
import com.epam.springweb.objects.UserDto;
import com.epam.springweb.service.ifaces.UserService;

public class UserServiceImpl implements UserService {

	private UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public void addUser(UserDto userDto) throws UserDaoException {
		userDao.insertUser(userDto);
	}

}
