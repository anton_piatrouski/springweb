package com.epam.springweb.service.impls;

import java.util.List;
import java.util.Set;

import com.epam.springweb.dao.ifaces.TopicDao;
import com.epam.springweb.hibernate.entities.Topic;
import com.epam.springweb.hibernate.entities.UserProfile;
import com.epam.springweb.service.ifaces.TopicService;

public class TopicServiceImpl implements TopicService {

	private TopicDao topicDao;

	public void setTopicDao(TopicDao topicDao) {
		this.topicDao = topicDao;
	}

	@Override
	public List<Topic> getTopics(UserProfile user, int page) {
		return topicDao.selectTopics(user, page);
	}

	@Override
	public List<Topic> searchTopics(String topicName, int page) {
		return topicDao.selectTopicsByName(topicName, page);
	}

	@Override
	public List<Topic> getUserTopics(String username) {
		return topicDao.selectUserTopics(username);
	}

	@Override
	public Topic getTopicById(int id) {
		return topicDao.selectTopicById(id);
	}

	@Override
	public Set<UserProfile> getTopicMembers(int id) {
		return topicDao.selectTopicMembers(id);
	}

	@Override
	public void addTopic(Topic topic) {
		topicDao.insertTopic(topic);
	}

	@Override
	public int getNumberOfTopics(UserProfile user) {
		return topicDao.selectNumberOfTopics(user);
	}

	@Override
	public int getNumberOfTopicsByName(String topicName) {
		return topicDao.selectNumberOfTopicsByName(topicName);
	}

	@Override
	public void removeTopicMember(UserProfile user, int topicId) {
		topicDao.deleteTopicMember(user, topicId);
	}

	@Override
	public void addTopicMember(UserProfile user, int topicId) {
		topicDao.insertTopicMember(user, topicId);
	}

	@Override
	public void modifyTopic(Topic topic) {
		topicDao.updateTopic(topic);
	}

}
