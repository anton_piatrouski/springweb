package com.epam.springweb.service.impls;

import java.util.List;

import com.epam.springweb.dao.ifaces.NotificationDao;
import com.epam.springweb.hibernate.entities.Notification;
import com.epam.springweb.service.ifaces.NotificationService;

public class NotificationServiceImpl implements NotificationService {

	private NotificationDao notificationDao;

	public void setNotificationDao(NotificationDao notificationDao) {
		this.notificationDao = notificationDao;
	}

	@Override
	public void addNotification(Notification notification) {
		notificationDao.insertNotification(notification);
	}

	@Override
	public void removeNotification(Notification notification) {
		notificationDao.deleteNotification(notification);
	}

	@Override
	public List<Notification> getNotifications(String username) {
		return notificationDao.selectNotifications(username);
	}

}
