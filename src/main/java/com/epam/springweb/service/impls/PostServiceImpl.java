package com.epam.springweb.service.impls;

import java.util.List;

import com.epam.springweb.dao.ifaces.PostDao;
import com.epam.springweb.hibernate.entities.Post;
import com.epam.springweb.service.ifaces.PostService;

public class PostServiceImpl implements PostService {

	private PostDao postDao;

	public void setPostDao(PostDao postDao) {
		this.postDao = postDao;
	}

	@Override
	public List<Post> getPosts(int topicId, int page) {
		return postDao.selectPosts(topicId, page);
	}

	@Override
	public Post getPost(int id) {
		return postDao.selectPost(id);
	}

	@Override
	public List<Post> getPostsByMember(String username, int page) {
		return postDao.selectPostsByMember(username, page);
	}

	@Override
	public void addPost(Post post) {
		postDao.insertPost(post);
	}

	@Override
	public int getCountPostsByMember(String username) {
		return postDao.countPostsByMember(username);
	}

}
