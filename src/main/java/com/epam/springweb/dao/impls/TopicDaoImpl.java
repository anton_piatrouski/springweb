package com.epam.springweb.dao.impls;

import java.util.List;
import java.util.Set;

import javax.persistence.NoResultException;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.epam.springweb.dao.ifaces.TopicDao;
import com.epam.springweb.hibernate.entities.Topic;
import com.epam.springweb.hibernate.entities.UserProfile;
import com.epam.springweb.utils.PaginationUtil;

public class TopicDaoImpl implements TopicDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<Topic> selectTopics(UserProfile user, int page) {
		Query<Topic> query = sessionFactory.getCurrentSession().createQuery(
				"select t from Topic t join t.members m where m.username = :name order by t.name asc", Topic.class);
		query.setParameter("name", user.getUsername());

		query.setFirstResult(PaginationUtil.calcFirstResult(page));
		query.setMaxResults(PaginationUtil.SIZE);

		return query.getResultList();
	}

	@Override
	public List<Topic> selectTopicsByName(String topicName, int page) {
		Query<Topic> query = sessionFactory.getCurrentSession().createQuery("select t from Topic t where "
				+ "t.privateStatus = :status and lower(t.name) like :name order by t.name asc", Topic.class);
		query.setParameter("status", Boolean.FALSE);
		query.setParameter("name", '%' + topicName.toLowerCase() + '%');

		query.setFirstResult(PaginationUtil.calcFirstResult(page));
		query.setMaxResults(PaginationUtil.SIZE);

		return query.getResultList();
	}

	@Override
	public List<Topic> selectUserTopics(String username) {
		UserProfile user = sessionFactory.getCurrentSession().load(UserProfile.class, username);
		List<Topic> topics = user.getTopics();
		return topics;
	}

	@Override
	public Topic selectTopicById(int id) {
		Topic topic = sessionFactory.getCurrentSession().get(Topic.class, id);
		return topic;
	}

	@Override
	public Set<UserProfile> selectTopicMembers(int id) {
		Topic topic = sessionFactory.getCurrentSession().load(Topic.class, id);
		return topic.getMembers();
	}

	@Override
	public void insertTopic(Topic topic) {
		sessionFactory.getCurrentSession().save(topic);
	}

	@Override
	public int selectNumberOfTopics(UserProfile user) {
		Query<Long> query = sessionFactory.getCurrentSession()
				.createQuery("select count(*) from Topic t join t.members m where m.username = :name", Long.class);
		query.setParameter("name", user.getUsername());

		return countRecords(query);
	}

	@Override
	public int selectNumberOfTopicsByName(String topicName) {
		Query<Long> query = sessionFactory.getCurrentSession().createQuery(
				"select count(*) from Topic t where t.privateStatus = :status and lower(t.name) like :name",
				Long.class);
		query.setParameter("status", Boolean.FALSE);
		query.setParameter("name", '%' + topicName.toLowerCase() + '%');

		return countRecords(query);
	}

	private int countRecords(Query<Long> query) {
		int number = 0;
		try {
			number = query.getSingleResult().intValue();
		} catch (NoResultException ex) {
			// do nothing
		}
		return number;
	}

	@Override
	public void deleteTopicMember(UserProfile user, int topicId) {
		Topic topic = sessionFactory.getCurrentSession().load(Topic.class, topicId);
		topic.removeMember(user);
	}

	@Override
	public void insertTopicMember(UserProfile user, int topicId) {
		Topic topic = sessionFactory.getCurrentSession().load(Topic.class, topicId);
		topic.addMember(user);
	}

	@Override
	public void updateTopic(Topic topic) {
		sessionFactory.getCurrentSession().update(topic);
	}

}
