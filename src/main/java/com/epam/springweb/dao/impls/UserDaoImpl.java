package com.epam.springweb.dao.impls;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.epam.springweb.dao.ifaces.UserDao;
import com.epam.springweb.exceptions.UserDaoException;
import com.epam.springweb.objects.Authority;
import com.epam.springweb.objects.UserDto;

public class UserDaoImpl implements UserDao {

	private static final String INSERT_USER = "INSERT INTO `users` (`username`, `password`, `enabled`) VALUES (:name, :password, :enabled)";
	private static final String INSERT_AUTHORITY = "INSERT INTO `authorities` (`username`, `authority`) VALUES (:name, :authority)";
	private static final String CHECK_USER = "SELECT `username` FROM `users` WHERE `username` = :name";

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void insertUser(UserDto userDto) throws UserDaoException {
		userDto.setEnabled(true);
		userDto.setAuthority(Authority.ROLE_USER);

		// throws exception if username is already exists
		checkUser(userDto.getName());

		@SuppressWarnings("rawtypes")
		Query query = sessionFactory.getCurrentSession().createNativeQuery(INSERT_USER);

		query.setParameter("name", userDto.getName());
		query.setParameter("password", userDto.getPassword());
		query.setParameter("enabled", userDto.isEnabled());
		query.executeUpdate();

		insertAuthority(userDto);
	}

	private void insertAuthority(UserDto userDto) {
		@SuppressWarnings("rawtypes")
		Query query = sessionFactory.getCurrentSession().createNativeQuery(INSERT_AUTHORITY);

		query.setParameter("name", userDto.getName());
		query.setParameter("authority", userDto.getAuthority().toString());

		query.executeUpdate();
	}

	private void checkUser(String name) throws UserDaoException {
		@SuppressWarnings("rawtypes")
		Query query = sessionFactory.getCurrentSession().createNativeQuery(CHECK_USER);
		query.setParameter("name", name);

		String result = (String) query.getSingleResult();

		if (name.equals(result)) {
			throw new UserDaoException("Username is already exists.");
		}
	}

}
