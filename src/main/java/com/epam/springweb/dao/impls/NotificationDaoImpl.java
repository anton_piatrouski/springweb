package com.epam.springweb.dao.impls;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;

import com.epam.springweb.dao.ifaces.NotificationDao;
import com.epam.springweb.hibernate.entities.Notification;

public class NotificationDaoImpl implements NotificationDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void insertNotification(Notification notification) {
		notification.setDate(new Date());

		sessionFactory.getCurrentSession().save(notification);
	}

	@Override
	public void deleteNotification(Notification notification) {
		sessionFactory.getCurrentSession().delete(notification);
	}

	@Override
	public List<Notification> selectNotifications(String username) {
		String queryString = "from Notification where receiver.username = :name order by date";

		TypedQuery<Notification> query = sessionFactory.getCurrentSession().createQuery(queryString,
				Notification.class);
		query.setParameter("name", username);

		return query.getResultList();
	}

}
