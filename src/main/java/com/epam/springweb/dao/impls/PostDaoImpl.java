package com.epam.springweb.dao.impls;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;

import com.epam.springweb.dao.ifaces.PostDao;
import com.epam.springweb.hibernate.entities.Post;
import com.epam.springweb.utils.PaginationUtil;

public class PostDaoImpl implements PostDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<Post> selectPosts(int topicId, int page) {
		String queryString = "from Post where topic.id = :id order by date";

		TypedQuery<Post> query = sessionFactory.getCurrentSession().createQuery(queryString, Post.class);
		query.setParameter("id", topicId);
		query.setFirstResult(PaginationUtil.calcFirstResult(page));
		query.setMaxResults(PaginationUtil.SIZE);

		return query.getResultList();
	}

	@Override
	public Post selectPost(int id) {
		return sessionFactory.getCurrentSession().get(Post.class, id);
	}

	@Override
	public List<Post> selectPostsByMember(String username, int page) {
		String queryString = "select distinct post from Post as post join post.topic.members as memb"
				+ " where memb.username = :name order by post.date";

		TypedQuery<Post> query = sessionFactory.getCurrentSession().createQuery(queryString, Post.class);
		query.setParameter("name", username);
		query.setFirstResult(PaginationUtil.calcFirstResult(page));
		query.setMaxResults(PaginationUtil.SIZE);

		return query.getResultList();
	}

	@Override
	public void insertPost(Post post) {
		sessionFactory.getCurrentSession().save(post);
	}

	@Override
	public int countPostsByMember(String username) {
		String queryString = "select distinct count(post) from Post as post join post.topic.members as memb"
				+ " where memb.username = :name";

		TypedQuery<Long> query = sessionFactory.getCurrentSession().createQuery(queryString, Long.class);
		query.setParameter("name", username);

		return query.getSingleResult().intValue();
	}

}
