package com.epam.springweb.dao.impls;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;

import com.epam.springweb.dao.ifaces.CommentDao;
import com.epam.springweb.hibernate.entities.Comment;

public class CommentDaoImpl implements CommentDao {

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void insertComment(Comment comment) {
		comment.setCreateDate(new Date());

		sessionFactory.getCurrentSession().save(comment);
	}

	@Override
	public List<Comment> selectComments(int postId) {
		String queryString = "from Comment where post.id = :id and parentComment is null order by createDate";

		TypedQuery<Comment> query = sessionFactory.getCurrentSession().createQuery(queryString, Comment.class);
		query.setParameter("id", postId);

		return query.getResultList();
	}

	@Override
	public void deleteComment(int id) {
		Comment comment = new Comment(id);
		sessionFactory.getCurrentSession().delete(comment);
	}

}
