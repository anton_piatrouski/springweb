package com.epam.springweb.dao.ifaces;

import java.util.List;
import java.util.Set;

import com.epam.springweb.hibernate.entities.Topic;
import com.epam.springweb.hibernate.entities.UserProfile;

public interface TopicDao {

	List<Topic> selectTopics(UserProfile user, int page);

	List<Topic> selectTopicsByName(String topicName, int page);

	List<Topic> selectUserTopics(String username);

	Topic selectTopicById(int id);

	Set<UserProfile> selectTopicMembers(int id);

	void insertTopic(Topic topic);

	int selectNumberOfTopics(UserProfile user);

	int selectNumberOfTopicsByName(String topicName);

	void deleteTopicMember(UserProfile user, int topicId);

	void insertTopicMember(UserProfile user, int topicId);

	void updateTopic(Topic topic);

}
