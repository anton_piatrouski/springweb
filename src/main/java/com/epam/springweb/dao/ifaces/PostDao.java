package com.epam.springweb.dao.ifaces;

import java.util.List;

import com.epam.springweb.hibernate.entities.Post;

public interface PostDao {
	List<Post> selectPosts(int topicId, int page);

	Post selectPost(int id);

	List<Post> selectPostsByMember(String username, int page);

	void insertPost(Post post);

	int countPostsByMember(String username);
}
