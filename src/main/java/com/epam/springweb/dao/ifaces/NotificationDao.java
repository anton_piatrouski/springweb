package com.epam.springweb.dao.ifaces;

import java.util.List;

import com.epam.springweb.hibernate.entities.Notification;

public interface NotificationDao {
	void insertNotification(Notification notification);

	void deleteNotification(Notification notification);

	List<Notification> selectNotifications(String username);
}
