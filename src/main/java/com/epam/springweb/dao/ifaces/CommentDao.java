package com.epam.springweb.dao.ifaces;

import java.util.List;

import com.epam.springweb.hibernate.entities.Comment;

public interface CommentDao {
	void insertComment(Comment comment);

	List<Comment> selectComments(int postId);

	void deleteComment(int id);
}
