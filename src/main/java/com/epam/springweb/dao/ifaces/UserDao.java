package com.epam.springweb.dao.ifaces;

import com.epam.springweb.exceptions.UserDaoException;
import com.epam.springweb.objects.UserDto;

public interface UserDao {
	void insertUser(UserDto userDto) throws UserDaoException;
}
