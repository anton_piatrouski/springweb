package com.epam.springweb.hibernate.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "topics")
public class Topic {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "topic_id")
	private Integer id;

	@Column(name = "topic_name")
	private String name;

	@Column(name = "description")
	private String description;

	@ManyToOne
	@JoinColumn(name = "username", foreignKey = @ForeignKey(name = "fk_topics_users"))
	private UserProfile creator;

	@Column(name = "private_status")
	private boolean privateStatus;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "members", joinColumns = {
			@JoinColumn(name = "topic_id", foreignKey = @ForeignKey(name = "fk_members_topics")) }, inverseJoinColumns = {
					@JoinColumn(name = "username", foreignKey = @ForeignKey(name = "fk_members_users")) })
	private Set<UserProfile> members = new HashSet<UserProfile>();

	public Topic() {

	}

	public Topic(int id) {
		this.id = id;
	}

	public void removeMember(UserProfile user) {
		members.remove(user);
	}

	public void addMember(UserProfile user) {
		members.add(user);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public UserProfile getCreator() {
		return creator;
	}

	public void setCreator(UserProfile creator) {
		this.creator = creator;
	}

	public boolean isPrivateStatus() {
		return privateStatus;
	}

	public void setPrivateStatus(boolean privateStatus) {
		this.privateStatus = privateStatus;
	}

	public Set<UserProfile> getMembers() {
		return members;
	}

	public void setMembers(Set<UserProfile> members) {
		this.members = members;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Topic))
			return false;
		Topic other = (Topic) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
