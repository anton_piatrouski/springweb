package com.epam.springweb.hibernate.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class UserProfile {

	@Id
	@Column(name = "username")
	private String username;

	@ManyToMany(mappedBy = "members", fetch = FetchType.EAGER)
	private List<Topic> topics = new ArrayList<Topic>();

	@OneToMany(mappedBy = "receiver")
	private Set<Notification> notifications = new HashSet<Notification>();

	public UserProfile() {

	}

	public void addNotification(Notification notification) {
		notifications.add(notification);
	}

	public void removeNotification(Notification notification) {
		notification.setReceiver(this);
		notifications.remove(notification);
	}

	public UserProfile(String username) {
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}

	public Set<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof UserProfile))
			return false;
		UserProfile other = (UserProfile) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

}
