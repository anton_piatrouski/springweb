package com.epam.springweb.hibernate.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "posts")
public class Post {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "post_id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "topic_id", foreignKey = @ForeignKey(name = "topic_id_fk"))
	private Topic topic;

	@ManyToOne
	@JoinColumn(name = "username", foreignKey = @ForeignKey(name = "username_fk"))
	private UserProfile creator;

	@Column(name = "text")
	private String text;

	@Column(name = "creation_date")
	private Date date;

	public Post() {

	}

	public Post(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public Topic getTopic() {
		return topic;
	}

	public UserProfile getCreator() {
		return creator;
	}

	public String getText() {
		return text;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getDate() {
		return date;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public void setCreator(UserProfile creator) {
		this.creator = creator;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
