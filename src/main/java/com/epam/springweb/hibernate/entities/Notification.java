package com.epam.springweb.hibernate.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "notifications")
public class Notification {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "notification_id")
	private Integer id;

	@OneToOne
	@JoinColumn(name = "topic_id")
	private Topic topic;

	@ManyToOne
	@JoinColumn(name = "sender_username")
	private UserProfile sender;

	@ManyToOne
	@JoinColumn(name = "receiver_username")
	private UserProfile receiver;

	@Column(name = "invitation_date")
	private Date date;

	public Notification() {

	}

	public Notification(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public Topic getTopic() {
		return topic;
	}

	public UserProfile getSender() {
		return sender;
	}

	public UserProfile getReceiver() {
		return receiver;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getDate() {
		return date;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public void setSender(UserProfile sender) {
		this.sender = sender;
	}

	public void setReceiver(UserProfile receiver) {
		this.receiver = receiver;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Notification))
			return false;
		Notification other = (Notification) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Notification [topic=" + topic.getId() + ", sender=" + sender.getUsername() + ", receiver="
				+ receiver.getUsername() + ", date=" + date + "]";
	}
}
