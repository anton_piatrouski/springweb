package com.epam.springweb.hibernate.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comments")
public class Comment {
	@Id
	@Column(name = "comment_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	@JoinColumn(name = "username")
	private UserProfile creator;

	@ManyToOne
	@JoinColumn(name = "post_id")
	private Post post;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentComment")
	private Comment parentComment;

	@OneToMany(mappedBy = "parentComment", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Comment> childComments;

	@Column(name = "createDate")
	private Date createDate;

	@Column(name = "text")
	private String text;

	public Comment() {

	}

	public Comment(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public UserProfile getCreator() {
		return creator;
	}

	public Post getPost() {
		return post;
	}

	public Comment getParentComment() {
		return parentComment;
	}

	public List<Comment> getChildComments() {
		return childComments;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreateDate() {
		return createDate;
	}

	public String getText() {
		return text;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCreator(UserProfile creator) {
		this.creator = creator;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public void setParentComment(Comment parentComment) {
		this.parentComment = parentComment;
	}

	public void setChildComments(List<Comment> childComments) {
		this.childComments = childComments;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "text=" + text;
	}
}
