$(document).ready(function() {
	$("#regForm").validate({
		rules: {
			username: {
				required: true,
				minlength: 5
			},
			password: {
				required: true,
				minlength: 3
			},
			confirm_password: {
				required: true,
				minlength: 3,
				equalTo: "#password"
			}
		},
		messages: {
			username: {
				required: "Please enter a username.",
				minlength: "Your username must consist of at least 5 characters."
			},
			password: {
				required: "Please enter a password.",
				minlength: "Your password must consist of at least 3 characters."
			},
			confirm_password: {
				required: "Please repeat a password.",
				minlength: "Your password must consist of at least 3 characters.",
				equalTo: "Please enter the same password."
			}
		}
	});
});
