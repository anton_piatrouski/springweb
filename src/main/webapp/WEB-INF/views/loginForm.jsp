<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Login</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/style/style.css" />">

<script src="<c:url value="/resources/bootstrap/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<body>
	<c:import url="content/header.jsp" />
	
	<div class="content">
		<div class="page-header">
			<h1>Login</h1>
		</div>
		<form name="loginForm" action="login" method="POST">
			<label for="">User</label>
			<div class="input-group">
				<input class="form-control" type="text" size="20"
					placeholder="username" name="loginUsername" required>
			</div>

			<label for="">Password</label>
			<div class="input-group">
				<input class="form-control" type="password" size="20"
					placeholder="password" name="loginPassword" required>
			</div>

			<c:if test="${not empty error}">
				<div class="alert alert-danger" role="alert">
					<c:out value="${error}" />
				</div>
			</c:if>
			
			<div>
				<input class="btn btn-success" type="submit" value="Send">
			</div>
		</form>
	</div>
</body>
</html>
