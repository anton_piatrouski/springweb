<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Registration</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/style/style.css" />">

<script src="<c:url value="/resources/bootstrap/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/resources/script/jquery.validate.js" />"></script>
<script src="<c:url value="/resources/script/registration-form.js" />"></script>
</head>
<body>
	<c:import url="content/header.jsp" />

	<div class="content">
		<div class="page-header">
			<h1>Registration</h1>
		</div>

		<form id="regForm" action="register" method="POST">
			<div class="form-group">
				<label for="">Username</label>
				<input type="text" id="username" name="username" class="form-control" 
					placeholder="Username">
			</div>
			<div class="form-group">
				<label for="">Password</label>
				<input type="password" id="password" name="password" class="form-control" 
					placeholder="Password">
			</div>
			<div class="form-group">
				<label for="">Repeat password</label>
				<input type="password" id="confirm_password" name="confirm_password" class="form-control" 
					placeholder="Repeat password">
			</div>
			<button type="submit" class="btn btn-success">Submit</button>
		</form>
		
		<c:if test="${not empty message}">
			<p class="bg-info alert">${message}</p>
		</c:if>

	</div>
</body>
</html>
