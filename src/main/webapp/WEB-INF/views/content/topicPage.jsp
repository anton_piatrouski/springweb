<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Topic page</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/style/style.css" />">

<script src="<c:url value="/resources/bootstrap/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<body>
	<%@ include file="header.jsp"%>

	<c:url var="modifyTopic" value="/forum/form-modify-topic/${topic.id}" />
	<c:url var="formAction" value="/forum/create-post/${topic.id}" />
	<c:url var="sendInvitation" value="/forum/send-invitation/${topic.id}" />

	<div class="content">
		<div class="page-header">
			<h1>
				<c:out value="${topic.name}" />
			</h1>
			<c:if test="${isCreator eq true}">
				<a class="btn btn-info" href="${modifyTopic}" role="button">Modify
					topic</a>

				<form:form class="form-inline" name="sendInvitationForm"
					modelAttribute="receiver" action="${sendInvitation}" method="POST">
					<div class="form-group">
						<label class="sr-only"></label>
						<form:input type="text" class="form-control" path="username"
							placeholder="Receiver username" required="required" />
					</div>
					<button type="submit" class="btn btn-default">Send
						invitation</button>
				</form:form>

			</c:if>
		</div>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>Text</th>
					<th>Creator name</th>
					<th>Creation date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${posts}">
					<c:url var="postPage" value="/forum/post/${item.id}" />
					<tr>
						<td>${item.text}</td>
						<td>${item.creator.username}</td>
						<td>${item.date}</td>
						<td><a href="${postPage}">post page</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<c:if test="${isMember eq true}">
			<form:form class="form-inline" name="createPostForm"
				modelAttribute="post" action="${formAction}" method="POST">
				<div class="form-group">
					<label class="sr-only"></label>
					<form:textarea rows="" cols="" class="form-control" path="text"
						placeholder="Message" required="required" />
				</div>
				<button type="submit" class="btn btn-default">Create new
					post</button>
			</form:form>
		</c:if>

	</div>
</body>
</html>
