<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Create form</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/style/style.css" />">

<script src="<c:url value="/resources/bootstrap/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<body>
	<c:import url="header.jsp" />

	<div class="content">
		<div class="page-header">
			<h1>Topic creation form</h1>
		</div>
		<form:form name="topicCreationForm" modelAttribute="topic" action="create-topic"
			method="POST">

			<div class="form-group">
				<label for="">Topic name</label>
				<form:input class="form-control" type="text" size="255"
					placeholder="" path="name" required="required" />
			</div>

			<div class="form-group">
				<label for="">Description</label>
				<form:input class="form-control" type="text" size="255"
					placeholder="" path="description" required="required"  />
			</div>

			<div class="checkbox">
				<label> <form:checkbox path="privateStatus" />
					Private
				</label>
			</div>

			<div>
				<input class="btn btn-success" type="submit" value="Send">
			</div>
		</form:form>
	</div>
</body>
</html>
