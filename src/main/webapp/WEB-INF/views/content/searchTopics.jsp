<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Search topics</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/style/style.css" />">

<script src="<c:url value="/resources/bootstrap/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<body>
	<%@ include file="header.jsp" %>

	<div class="content">
		<div class="page-header">
			<h1>Search topics</h1>
		</div>

		<form class="form-inline" name="searchTopicForm" action="<c:url value="/forum/search-topics/results" />" method="GET">

			<div class="form-group">
				<input class="form-control" type="text" size=""
					placeholder="Topic name" name="topicName" value="${topicName}" />
			</div>
			<button type="submit" class="btn btn-success">Search</button>
		</form>

		<c:if test="${not empty topics}">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Topic name</th>
						<th>Description</th>
						<th>Creator name</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="topic" items="${topics}">
					<c:set var="creator" value="${topic.creator.username}" />
					<c:set var="canLeave" value="false" />
						
					<c:forEach var="userTopic" items="${userTopics}">
						<c:if test="${userTopic.id eq topic.id}"><c:set var="canLeave" value="true" /></c:if>
					</c:forEach>
						
					<tr>
						<td><a href="<c:url value="/forum/topic/${topic.id}" />">${topic.name}</a></td>
						<td>${topic.description}</td>
						<td>${creator}</td>
						<td>
						<c:if test="${canLeave eq 'true' and creator ne username}">
							<a href="<c:url value="/forum/leave-topic/${topic.id}" />">leave</a>
						</c:if>
						<c:if test="${canLeave eq 'false'}">
							<a href="<c:url value="/forum/join-topic/${topic.id}" />">join</a>
						</c:if>
						</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>

			<div class="pagination">
				<c:forEach var="page" begin="1" end="${pages}">
					<c:choose>
						<c:when test="${page eq currentPage}">
							<a class="btn btn-default active"
								href="<c:url value="?topicName=${topicName}&page=${page}" />" role="button">${page}</a>
						</c:when>
						<c:otherwise>
							<a class="btn btn-default"
								href="<c:url value="?topicName=${topicName}&page=${page}" />" role="button">${page}</a>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</div>
		</c:if>
		
		<c:if test="${not empty message}">
			<p class="bg-info alert">${message}</p>
		</c:if>
	</div>
</body>
</html>
