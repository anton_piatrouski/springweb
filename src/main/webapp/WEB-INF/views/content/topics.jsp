<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Topics</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/style/style.css" />">

<script src="<c:url value="/resources/bootstrap/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<body>
	<%@ include file="header.jsp" %>

	<div class="content">
		<div class="page-header">
			<h1>Topics</h1>
			<a class="btn btn-info" href="<c:url value="/forum/form-create-topic" />"
				role="button">Create topic</a>
		</div>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>Topic name</th>
					<th>Description</th>
					<th>Creator name</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="topic" items="${topics}">
					<tr>
						<td><a href="<c:url value="/forum/topic/${topic.id}" />">${topic.name}</a></td>
						<td>${topic.description}</td>
						<td>${topic.creator.username}</td>
						<td>
							<c:if test="${topic.creator.username ne username}">
								<a href="<c:url value="/forum/leave-topic/${topic.id}" />">leave</a>
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<div class="pagination">
			<c:forEach var="page" begin="1" end="${pages}">
				<c:choose>
					<c:when test="${page eq currentPage}">
						<a class="btn btn-default active"
							href="<c:url value="/forum?page=${page}" />" role="button">${page}</a>
					</c:when>
					<c:otherwise>
						<a class="btn btn-default"
							href="<c:url value="/forum?page=${page}" />" role="button">${page}</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</div>
	</div>
</body>
</html>
