<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Notification page</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/style/style.css" />">

<script src="<c:url value="/resources/bootstrap/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<body>
	<%@ include file="header.jsp"%>

	<div class="content">
		<div class="page-header">
			<h1>Notifications</h1>
		</div>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>Topic name</th>
					<th>Who invited</th>
					<th>Invitation date</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="item" items="${notifications}">
					<c:url var="acceptInvite"
						value="/forum/accept-invite/${item.id}/${item.topic.id}" />
					<c:url var="declineInvite"
						value="/forum/decline-invite/${item.id}" />
					<tr>
						<td>${item.topic.name}</td>
						<td>${item.sender.username}</td>
						<td>${item.date}</td>
						<td>
							<form action="${acceptInvite}" method="post">
								<button type="submit" class="btn btn-link">Accept
									invite</button>
							</form>
						</td>
						<td>
							<form action="${declineInvite}" method="post">
								<button type="submit" class="btn btn-link">Decline
									invite</button>
							</form>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>
</body>
</html>
