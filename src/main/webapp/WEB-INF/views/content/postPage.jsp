<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Post page</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/style/style.css" />">

<script src="<c:url value="/resources/bootstrap/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<body>
	<%@ include file="header.jsp"%>

	<c:url var="addComment" value="/forum/post/${post.id}/add-comment" />

	<div class="content">
		<div class="page-header">
			<h1>Post page</h1>
		</div>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>Text</th>
					<th>Creator name</th>
					<th>Creation date</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>${post.text}</td>
					<td>${post.creator.username}</td>
					<td>${post.date}</td>
				</tr>
			</tbody>
		</table>

		<div class="page-delimiter"></div>

		<c:if test="${not empty comments}">
			<c:forEach var="mainComment" items="${comments}">

				<c:set var="childComments1" value="${mainComment.childComments}" />

				<div class="main-comment">
					<table class="table table-condensed comment">
						<tr>
							<td>${mainComment.creator.username}</td>
							<td>${mainComment.createDate}</td>
						</tr>
						<tr>
							<td colspan="2">${mainComment.text}</td>
						</tr>
						<tr>
							<td colspan="2"><c:if
									test="${empty childComments1 and canAddComment eq true}">
									<c:url var="addChildComment"
										value="/forum/post/${post.id}/add-comment/${mainComment.id}" />
									<form:form class="form-inline" name="addCommentForm"
										modelAttribute="comment" action="${addChildComment}"
										method="POST">
										<div class="form-group">
											<label class="sr-only"></label>
											<form:textarea rows="" cols="" class="form-control"
												path="text" placeholder="Comment" required="required" />
										</div>
										<button type="submit" class="btn btn-link">Answer</button>
									</form:form>
								</c:if></td>
						</tr>
					</table>

					<c:forEach var="childComment1" items="${childComments1}">

						<c:set var="childComments2" value="${childComment1.childComments}" />

						<div class="child-comment">
							<table class="table table-condensed comment">
								<tr>
									<td>${childComment1.creator.username}</td>
									<td>${childComment1.createDate}</td>
								</tr>
								<tr>
									<td colspan="2">${childComment1.text}</td>
								</tr>
								<tr>
									<td colspan="2"><c:if
											test="${empty childComments2 and canAddComment eq true}">
											<c:url var="addChildComment"
												value="/forum/post/${post.id}/add-comment/${childComment1.id}" />
											<form:form class="form-inline" name="addCommentForm"
												modelAttribute="comment" action="${addChildComment}"
												method="POST">
												<div class="form-group">
													<label class="sr-only"></label>
													<form:textarea rows="" cols="" class="form-control"
														path="text" placeholder="Comment" required="required" />
												</div>
												<button type="submit" class="btn btn-link">Answer</button>
											</form:form>
										</c:if></td>
									<td colspan="2"><c:if
											test="${canDeleteComment eq true}">
											<c:url var="deleteComment"
												value="/forum/post/${post.id}/delete-comment/${childComment1.id}" />
											<form:form class="form-inline"
												action="${deleteComment}"
												method="POST">
												<button type="submit" class="btn btn-link">delete comment</button>
											</form:form>
										</c:if></td>
								</tr>
							</table>

							<c:forEach var="childComment2" items="${childComments2}">
								<div class="child-comment">
									<table class="table table-condensed comment">
										<tr>
											<td>${childComment2.creator.username}</td>
											<td>${childComment2.createDate}</td>
										</tr>
										<tr>
											<td colspan="2">${childComment2.text}</td>
										</tr>
									</table>
								</div>
							</c:forEach>
						</div>
					</c:forEach>
				</div>
			</c:forEach>
		</c:if>

		<c:if test="${canAddComment eq true}">
			<form:form class="form-inline" name="addCommentForm"
				modelAttribute="comment" action="${addComment}" method="POST">
				<div class="form-group">
					<label class="sr-only"></label>
					<form:textarea rows="" cols="" class="form-control" path="text"
						placeholder="Comment" required="required" />
				</div>
				<button type="submit" class="btn btn-default">Add comment</button>
			</form:form>
		</c:if>

	</div>
</body>
</html>
