<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Feed</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/style/style.css" />">

<script src="<c:url value="/resources/bootstrap/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<body>
	<%@ include file="header.jsp"%>

	<div class="content">
		<div class="page-header">
			<h1>Feed</h1>
		</div>

		<c:if test="${not empty posts}">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Topic name</th>
						<th>Topic description</th>
						<th>Post creator</th>
						<th>Creation date</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="post" items="${posts}">
						<c:url var="postPage" value="/forum/post/${post.id}" />
						<tr>
							<td>${post.topic.name}</td>
							<td>${post.topic.description}</td>
							<td>${post.creator.username}</td>
							<td>${post.date}</td>
							<td><a href="${postPage}">post page</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
		
		<div class="pagination">
			<c:forEach var="page" begin="1" end="${pages}">
				<c:choose>
					<c:when test="${page eq currentPage}">
						<a class="btn btn-default active" href="?page=${page}">${page}</a>
					</c:when>
					<c:otherwise>
						<a class="btn btn-default" href="?page=${page}">${page}</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</div>

	</div>
</body>
</html>
