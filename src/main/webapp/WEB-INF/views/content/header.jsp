<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<sec:authorize access="isAuthenticated()">
	<sec:authentication var="username" property="principal.username" />
</sec:authorize>

<nav class="navbar navbar-default navbar-static-top">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toogle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
<%-- 			<a class="navbar-brand" href="<c:url value="/" />">home</a> --%>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<c:choose>
					<c:when test="${empty username}">
						<li><a href="<c:url value="/form-login" />">Login</a></li>
						<li><a href="<c:url value="/form-registration" />">Register</a></li>
					</c:when>
					<c:otherwise>
						<li><a href="<c:url value="/forum/feed" />">Feed</a></li>
						<li><a href="<c:url value="/forum" />">Topics</a></li>
						<li><a href="<c:url value="/forum/notifications" />">Notifications</a></li>
						<li><a href="<c:url value="/forum/search-topics" />">Search</a></li>
						<li><a href="<c:url value="/forum/edit-profile" />">Edit Profile</a></li>
						<li><a href="<c:url value="/logout" />">Logout</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
	</div>
</nav>
