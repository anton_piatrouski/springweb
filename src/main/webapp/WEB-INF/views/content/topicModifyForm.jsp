<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Modify topic</title>
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap-theme.min.css" />">
<link rel="stylesheet"
	href="<c:url value="/resources/style/style.css" />">

<script src="<c:url value="/resources/bootstrap/js/jquery.min.js" />"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<body>
	<%@ include file="header.jsp"%>

	<c:url var="formAction" value="/forum/modify-topic" />

	<div class="content">
		<div class="page-header">
			<h1>Modify topic</h1>
		</div>

		<form:form name="topicModifyForm" modelAttribute="modifiedTopic"
			action="${formAction}" method="POST">
			<form:hidden path="id" value="${topic.id}" />
			<div class="form-group">
				<label for="">Topic name</label>
				<form:input type="text" path="name" value="${topic.name}"
					class="form-control" placeholder="Topic name" required="required" />
			</div>
			<div class="form-group">
				<label for="">Description</label>
				<form:input type="text" path="description"
					value="${topic.description}" class="form-control"
					placeholder="Description" required="required" />
			</div>
			<c:if test="${topic.privateStatus eq true}">
				<div class="checkbox">
					<label>
						<form:checkbox path="privateStatus" checked="checked" /> Private
					</label>
				</div>
			</c:if>
			<c:if test="${topic.privateStatus eq false}">
				<div class="checkbox">
					<label>
						<form:checkbox path="privateStatus" /> Private
					</label>
				</div>
			</c:if>
			<button type="submit" class="btn btn-success">Submit</button>
		</form:form>

	</div>
</body>
</html>
